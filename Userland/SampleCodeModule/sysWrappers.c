#include "sysWrappers.h"

void printMemory(){
	os_print_memory();
}

void sleep(int timeUnit){
	os_sleep(timeUnit);
}

void drawBlackRect (uint64_t x, uint64_t y){	
	os_draw(x, y, 0, 0, 0);
}


void createProcess(char * name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground, uint64_t fdin, uint64_t fdout){
	processInfo process;
	process.name = name;
	process.rip = rip;
	process.priority = priority;
	process.memSize = memSize;
	process.foreground = foreground;
	process.fdInput = fdin;
	process.fdOutput = fdout;

	os_create(&process);
}

void changeProcessPriority (uint64_t pid, uint64_t newPriority){
	os_change_priority(pid, newPriority);
}

uint64_t getPid(){
	return os_get_pid();
}

void printPCB(void){
    os_print_PCB();
    return;
}

void popen (uint64_t fd[2]){
	os_popen(fd);
}

void pclose (uint64_t fd){
	os_pclose(fd);
}

