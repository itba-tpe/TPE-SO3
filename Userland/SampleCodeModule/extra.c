#include "extra.h"

void whileOne(){
	printf("\nProgram eternally loops\n");
	while (1){
		//DO ABSOLUTELY NOTHING
	}
}

//Program will block and will stay blocked eternally -> To show the ps working
void doubleLock(){
	printf("\nProgram will wait twice on the same mutex, locking it\n");
	mutex m = getMutex("doubleLock");
	wait(m);
	wait(m);
}

void printEnteredText (void){
	char buffer[MAX_CHARACTERS];

    //these printf interfere with the pipe implementation
//    printf("\nWrite a sentence no longer than ");
//    printInt(MAX_CHARACTERS);
//    printf(" characters: ");

	hidden_scanf(buffer, MAX_CHARACTERS);
	printf(buffer);
	printf("\n");
}

void printInvertedText (void){
	char buffer[MAX_CHARACTERS];
	char inverted[MAX_CHARACTERS];

//    printf("\nWrite a sentence no longer than ");
//    printInt(MAX_CHARACTERS);
//    printf(" characters: ");

	hidden_scanf(buffer, MAX_CHARACTERS);

	printf(reverseString(buffer, inverted));
	printf("\n");
}
