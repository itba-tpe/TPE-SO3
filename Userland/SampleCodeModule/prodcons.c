#include "prodcons.h"
#include "stdio.h"

int run;
int prodIndex;
int prodAmount;
int prodAmountWaiting;
int consIndex;
int consAmount;
int consAmountWaiting;

static mutex lock;
static mutex prodWaiting;
static mutex consWaiting;

void initProdCons(){
	run = 1;

	intro();
	resetVariables();

	uint64_t fdPipe = open(PIPE);
	lock = getMutex("prodConsMutex");
	prodWaiting = getMutex("prodMutex");
	consWaiting = getMutex("consMutex");
    
    post(lock);     //dark arts

	char c;
	while (run){
		c = getChar();
		if (c == 'q'){
			run = 0;
			sleep(1);
		} else if (c == 'p'){
			createProducer();
		} else if (c == 'c'){
			createConsumer();
		}
	}

	deleteMutex(lock);
	deleteMutex(prodWaiting);
	deleteMutex(consWaiting);
	close(fdPipe);
	return;
}

void intro(){
	printf("\nApplication will create producers and consumer that wish to access the same pipe\n");
	printf("Press p to create a producer\n");
	printf("Press c to create a consumer\n");
	printf("Press q to quit at any time\n");
	printf("\n");
	return;
}

void createProducer(){
	wait(lock);
	if (prodIndex < MAX_PROD){
		printf("Producer #");
		printInt(++prodIndex);
		printf(" created\n");
		createProcess("producer", (uint64_t)&producer, MIN_PRIORITY, ONE_BLOCK, BACKGROUND, STDIN, STDOUT);
	} else {
		post(lock);
		printf("Maximum amount of producers reached\n");
	}
	return;
}

void createConsumer(){
	wait(lock);
	if (consIndex < MAX_CONS){
		printf("Consumers remaining = ");
		printInt(++consIndex);
		printf("\n");
		createProcess("consumer", (uint64_t)&consumer, MIN_PRIORITY, ONE_BLOCK, BACKGROUND, STDIN, STDOUT);
	} else {
		post(lock);
		printf("Maximum amount of consumers reached\n");
	}
	return;
}

void producer() {
    uint64_t id = prodIndex;
    post(lock);

    int fdPipe = open(PIPE);
   	char * msg = "This message is from the producers and will be read by the consumers";

    do {
		wait(lock);
		while (prodAmount != 0){
			prodAmountWaiting++;
			post(lock);
			// If there is another process waiting to write, the process will block here
			wait(prodWaiting);
			wait(lock);
		}
		if (prodAmountWaiting > 0) prodAmountWaiting--;
		prodAmount++;
		post(lock);
		
		write(fdPipe, msg, strlen(msg) + 1);
		printf("Producer ");
		printInt(id);
		printf(" wrote: ");
		printf(msg);
		printf("\n\n");

		wait(lock);
		prodAmount--;
		post(lock);

		if(consAmountWaiting > 0){
			// If there were consumers waiting, unblock them all so they can consume
			for(int i = consAmountWaiting; i > 0; i--){
				post(consWaiting);
			}
		} else {
			// If there are any producers waiting, unblock the first one
			if (prodAmountWaiting > 0){
				post(prodWaiting);
			}
		}
		sleep(1);
	} while(run);
  
    close(fdPipe);
	return;
}

void consumer() {
    uint64_t id = consIndex;
	post(lock);

    int fdPipe = open(PIPE);
	char buff[BUFFSIZE];

	do {
		wait(lock);
		while (prodAmount != 0){
			consAmountWaiting++;
			post(lock);
			wait(consWaiting);
			wait(lock);
		}
		if(consAmountWaiting > 0) consAmountWaiting--;
		consAmount++;
		post(lock);

		read(fdPipe, buff, BUFFSIZE-1);
		buff[BUFFSIZE-1] = '\0';
	    
		printf("Consumer ");
		printInt(id);
		printf(" read: ");
		printf(buff);
		printf("\n\n");

		wait(lock);
		consAmount--;
		post(lock);


		//If there are no consumers and ther ARE producers waiting, unblock a producer
		if(consAmount == 0 && prodAmountWaiting != 0){
			post(prodWaiting);
		}

		sleep(1);
	} while (run);

    close(fdPipe);
	return;
}

void resetVariables(){
	run = 1;
    prodIndex = 0;
	prodAmount = 0;
	prodAmountWaiting = 0;
	consIndex = 0;
	consAmount = 0;
	consAmountWaiting = 0;
}
