#include "memoryDemo.h"

void memoryDemo(){
	uint64_t pointers[MEM_ALLOCATED_AMOUNT];
	uint64_t memAllocated[MEM_ALLOCATED_AMOUNT];
	int i, j, mem = INITIAL_MEM;

	// Allocates MEM_ALLOCATED AMOUNT sections of memory with different sizes
	for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
		if((pointers[i] = (uint64_t)os_malloc(mem + i * OFFSET)) == (uint64_t)-1){
			// Must release all the previous pointers
			int j;
			printf("\nERROR: Insufficient memory to allocate\n");
			for (j = i - 1; j >= 0; j--){
				os_free((void *)pointers[j]);
			}
			return;
		} 
		// Saving the size of the pointer which was successfully created
		// Finding the amount of blocks needed for specified size

		uint64_t size = mem + i * OFFSET;
		uint64_t blocks = 0;
		while (size > ONE_BLOCK){
			size -= ONE_BLOCK;
			blocks++;
		}
		if (size != 0) blocks++;

		memAllocated[i] = blocks * ONE_BLOCK;
	}

	printf("\nAll pointers successfully created!\n");
	printf("\nComparing the pointers with one another...\n");

	sleep(1);
	printMemory();

	// Compares to see that all the returned pointers are unique
	for (i = 0; i < MEM_ALLOCATED_AMOUNT - 1; i++){
		for (j = i + 1; j < MEM_ALLOCATED_AMOUNT; j++){

			// If the pointer j is in the area that was allocated for pointer i, they overlap
			if (pointers[j] >= pointers[j] && pointers[j] < pointers[i] + memAllocated[i]){
				printf("\nERROR: Pointers overlap!");
				// Free all the pointers
				for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
					os_free((void *)pointers[i]);
				}
				return;
			}
		}
	}
	printf("\nSUCCESS: No pointers overlap!\n");

	printf("\nFreeing all pointers...\n");

	// Frees all the allocated memory
	for (i = 0; i < MEM_ALLOCATED_AMOUNT; i++){
		os_free((void *)pointers[i]);
	}
	printf("\nFinished.\n");
	return;
}


void testBuddyAllocation(){
	uint64_t index;
	uint64_t ptrs[PTR_AMOUNT];
	uint64_t blockAmount;

	printf("\n\nBefore allocating 10 blocks, 20 blocks, 30 blocks, 40 blocks, 50 blocks, and 60 blocks of 4KB:");
	printMemory();

	for (index = 0, blockAmount = 10; index < PTR_AMOUNT; index++){
		if((ptrs[index] = allocatePtr(blockAmount)) == (uint64_t)-1 ){
			return;
		}
		blockAmount += BLOCK_OFFSET;
	}

	printf("\nSleeping...\n");
	sleep(3);
	printf("\n\nAfter allocating 10 blocks, 20 blocks, 30 blocks, 40 blocks, 50 blocks, and 60 blocks of 4KB:");
	printMemory();

	freePtrs(ptrs, PTR_AMOUNT);
	
	printf("\nSleeping...\n");
	sleep(3);
	printf("\n\nAfter freeing the created pointers and merging the partitions:");
	printMemory();
}

uint64_t allocatePtr (uint64_t blockAmount){
	uint64_t ptr = (uint64_t)os_malloc(blockAmount * ONE_BLOCK);
	if( ptr == (uint64_t)-1){
		// Must release pointer
		printf("\nERROR: Insufficient memory to allocate\n");
		os_free((void *)ptr);
		return -1;
	}
	return ptr;
}

void freePtrs (uint64_t ptrs[], uint64_t length){
	uint64_t i;

	for (i = 0; i < length; i++){
		os_free((void *)ptrs[i]);
	}
	return;
}
