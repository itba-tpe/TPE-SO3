#include "pipes.h"

uint64_t open(char * name){
	return os_open_pipe(name);
}

void close(uint64_t fd){
    os_pclose(fd);
}
