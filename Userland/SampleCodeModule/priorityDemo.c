#include "priorityDemo.h"

static mutex lockA;
static mutex lockB;

void priorityDemo(){
	lockA = getMutex("lockA");
	lockB = getMutex("lockB");
	//stdinMutex = getMutex("stdinMutex");

	introMessage();

	createProcess("ProcessA", (uint64_t)&processA, 10, ONE_BLOCK, FOREGROUND, STDIN, STDOUT);
	createProcess("ProcessB", (uint64_t)&processB, 10, ONE_BLOCK, FOREGROUND, STDIN, STDOUT);
}

void processA(){
	wait(lockA);
	printLetter(PROCESSA_ID);
	post(lockA);

	// Will block until processB is finished printing letters
	wait(lockB);
	post(lockB);

	wait(lockA);
	changeProcessPriority(getPid(), getPriority(PROCESSA_ID));
	post(lockA);

	//Want both processes to start printing letters at the same time
	wait(lockB);
	post(lockB);

	printLetter(PROCESSA_ID);

	//Both process will be done using the mutexes when they reach printLetter
	// (which they do together), so it doesnt matter who deletes the mutexes
	deleteMutex(lockA);
	deleteMutex(lockB);
}


void processB(){
	wait(lockB);
	printLetter(PROCESSB_ID);
	post(lockB);

	// Will block until processA is finished printing letters
	wait(lockA);
	post(lockA);

	wait(lockB);
	changeProcessPriority(getPid(), getPriority(PROCESSB_ID));
	post(lockB);

	//Want both processes to start printing letters at the same time
	wait(lockA);
	post(lockA);

	printLetter(PROCESSB_ID);

}

void printLetter(char * letter){
	uint64_t index;
	for (index = 0; index < MAXCHARACTER; index++){
		printf(" ");
		printf(letter);
		printf(" ");
		miniSleep();
	}
}

uint64_t getPriority(char * id){
	if (strcmp(id, PROCESSA_ID) == 0){
		printf("\nProcessA priority changes from 10 to 5\n");
		return MAX_PRIORITY + 5;
	}
	printf("\nProcessB priority changes from 10 to 15\n");
	return MIN_PRIORITY - 5;
}

void introMessage(){
	printf("ProcessA --> Prints ");
	printf(PROCESSA_ID);
	printf(" and has a priority of 10\n");
	printf("ProcessB --> Prints ");
	printf(PROCESSB_ID);
	printf(" and has a priority of 10\n");
}

void miniSleep(){
	uint64_t i, j;
	for (i = 0; i < SLEEP; i++){
		for (j = 0; j < SLEEP; j++){
			//Just sleep
		}
	}
}

/*
void syncPriorityChange(char * id){
	//Cannot have both processes requesting input at the same time
	wait(stdinMutex);
	changeProcessPriority(getPid(), getPriority(id));
	post(stdinMutex);

	//Want to start printing letters at the same time -> will wait till other program finishes writing input if i did it first
	wait(stdinMutex);
	post(stdinMutex);
	return;
}

uint64_t getPriority(char * id){
	
	char buf[PRIORITY_SIZE + 1];

	printf("\nEnter new priority for process");
	printf(id);
	printf(": ");
	scanf(buf, PRIORITY_SIZE + 1);
	buf[PRIORITY_SIZE] = '\0';
	printf("\n");
	
	return stringToInt(buf);
}
*/
