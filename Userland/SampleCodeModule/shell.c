#include "shell.h"

static int exit = 0;
static mutex foreground;
static cmd firstCommand;
static char * c[PCOUNT] = { "help",
                            "time" ,
                            "pong" ,
                            "ps" ,
                            "prodcons",
                            "clear",
                            "exit",
                            "pipeDemo",
                            "memoryDemo",
                            "whileOne",
                            "doubleLock",
                            "printMemory",
                            "nice",
                            "printOriginal",
                            "printReverse",
                            "buddyDemo", 
                            "priorityDemo",
                            "printPCB",
                            "philosophers"
};

static uint64_t p[PCOUNT] = {(uint64_t)&help, 
                             (uint64_t)&showTime, 
                             (uint64_t)&pong, 
                             (uint64_t)&ps, 
                             (uint64_t)&initProdCons, 
                             (uint64_t)&clear,
                             0,
                             (uint64_t)&initPipesDemo, 
                             (uint64_t)&memoryDemo, 
                             (uint64_t)&whileOne, 
                             (uint64_t)&doubleLock, 
                             (uint64_t)&printMemory,
                             0,
                             (uint64_t)&printEnteredText,
                             (uint64_t)&printInvertedText,
                             (uint64_t)&testBuddyAllocation,
                             (uint64_t)&priorityDemo,
                             (uint64_t)&printPCB,
                             (uint64_t)&initPhilosophers
};

void shell_init() {
	//Start Shell
	foreground = getMutex("foreground");
	wait(foreground);
	static char command[MAX_COMMAND_LENGTH];
	//int exit = 0;
	printf("\nARQ TPE Group 2");
	printf("\nWhat module would you like to execute? (try 'help')");
	while (!exit) {
		printf("\n$>");
		scanf(command, MAX_COMMAND_LENGTH);
		exit = shell_execute(command);
	}
	printf("\nGoodbye.");
	return;
}

int shell_execute(char *command) {
	    
    //Now we need to compare the command to all the possible options
    if (*command == -1) {
        printf("\n^C");
        return exit;
    }
    printf("\n");

    
    cmd *current = &firstCommand;
    int exec = 1;
    
    //fd gets the new output, and saves the input on input
    uint64_t fd[2] = {STDIN, STDOUT};
    uint64_t input = STDIN;
    
    if( getCommand(command) ){
        while(current != '\0' && exec && !exit){
            
            if(current->next != '\0'){
                popen(fd);
            }else{
                fd[1] = 1;
            }
            
            exec = runCommand(current, input, fd[1]);
            
            //closing STDIN & STDOUT should be legit, but here we shouldn't
            if(input != STDIN)
                pclose(input);
            if(fd[1] != STDOUT)
                pclose(fd[1]);
            
            current = current->next;
            input = fd[0];
            
        }
    }
    
    freeCommands();

    
    return exit;
    
}

int getCommand(char* command){
    
    int iter = 0;
    int exec = 1;
    cmd * current = &firstCommand;
    cmd *newCommand;
    char buffer[MAX_COMMAND_LENGTH];
    int b = 0;
    
    //IMPROVISING
    
    while (exec == 1 && command[iter] != '\0' ){
        
        //trim the beginning
        while(command[iter] == ' ') {iter++;}
        
        if(iter > MAX_COMMAND_LENGTH){
            exec = 0;
            continue;
        }
        
        while(command[iter] != PIPECHAR && command[iter] != '\0'){
            
            buffer[b++] = command[iter++];
            
            if(iter > MAX_COMMAND_LENGTH){
                exec = 0;
                continue;
            }
        }
        
        //end or pipe
        //if pipe check now, if end, let the while handle it
        if(command[iter] == PIPECHAR){   //create pipes later, when attempting to create the processes
            
            iter++;
            buffer[b] = '\0';
            b = 0;
            //printf("\n");
            //printf(buffer);
            exec = validateCommand(buffer,current);
            
            
            if(exec == 1){
                newCommand = os_malloc(sizeof(command));
                current->next = newCommand;
                current = current->next;
                current->next = '\0';
            }
        }
    }
    //got to end of string or exec is 0

    if(exec == 0){freeCommands();}
    
    if(command[iter] == '\0'){
        buffer[b] = '\0';
        exec = validateCommand(buffer,current);
        if(exec == 0){freeCommands();}
    }
    
    return exec;
}

//searches in command array if it matches
int validateCommand(char *command, cmd *process){
    
    int exec = 1;
    
    process->fg = isForeground(command);

    int exist = -1;          //if it matches an existing command

    //exist check
    for(int i = 0 ; i < PCOUNT && exist == -1 ; i++){
        if(i == NICE){
            exist = isNice(command,process);
        }else if(strcmp(command, c[i]) == 0){
            exist = i;
        }
    }

    

    process->commandId = exist;
    
    switch (exist) {
            
            //both foreground and background
        case HELP:
        case TIME:
        case PS:
        case MEMORYDEMO:
        case PRINTMEM:
            process->priority = MIN_PRIORITY;
            process->stackSize = SHELL_STACK_SIZE;
            break;
            
            
            //foreground only
        case PONG:
        case PRODCONS:
        case PIPEDEMO:
        case PRINTTEXT:
        case PRINTREVERSE:
        case BUDDYDEMO:
        case PRIORITYDEMO:
        case PHILOSOPHERS:
            if(process->fg == SHELL_FOREGROUND){
                process->priority = MAX_PRIORITY;
                process->stackSize = SHELL_STACK_SIZE;
            }else{
                exec = 0;
                printf("\n");
                printf(c[process->commandId]);
                printf(" is not supported as a background process");
            }
            break;
            
            //these don't generate processes
        case CLEAR:
        case EXIT:
        case NICE:
        case PRINTPCB:
            if(process->fg == SHELL_FOREGROUND){
                process->fg = !SHELL_FOREGROUND;
            }else{
                exec = 0;
                printf("\nProcess ");
                printf(c[process->commandId]);
                printf("can not be run in the background");
            }
            break;
            
            //small processes for demonstations and debugging
        case WHILEONE:
        case DOUBLELOCK:
            process->priority = MIN_PRIORITY;
            process->stackSize = ONE_BLOCK;
            break;
            
            
        default:
            printf("\nshell: ");
            printf(command);
            printf(": command not found (try help)");
            exec = 0;
            process->fg = !SHELL_FOREGROUND;
            break;
    }

    return exec;
}

//frees all commands except the first one
void freeCommands(void){
    cmd * current = &firstCommand;
    cmd * next = firstCommand.next;
    
    while(next != '\0'){
        current = next;
        next = current->next;
        os_free(current);
    }
    if(current != &firstCommand){   //the last one, as he has next == null, but wasn't yet freed.
        os_free(current);
    }
    firstCommand.next = '\0';
    
}

//trims command and returns if it is foreground or background
int isForeground(char * command){
    int fg;             //if it is foreground or background

    //fg check (and cut)
    trim(command);
    int len = strlen(command) - 1;
    if(command[len] == '&'){
        fg = !SHELL_FOREGROUND;
        command[len] = '\0';
    }else{
        fg = SHELL_FOREGROUND;
    }
    trim(command);

    return fg;
}

int runCommand(cmd *command, int input, int output){
    //adapt to the pipes
    
    if(command->commandId == EXIT){
        exit = 1;
    } else if(  command->commandId == CLEAR     ||
                command->commandId == PRINTPCB  ){
        
        void (*function)(void) = (void*) p[command->commandId];

        function();
        
    } else if(command->commandId == NICE){
        changeProcessPriority(command->stackSize, command->priority);
    }else{
        createProcess(c[command->commandId],p[command->commandId], command->priority, command->stackSize, command->fg, input, output);
    }
    
    if(command->fg == SHELL_FOREGROUND){
        wait(foreground);
    }
    return 1;
}

int isNice(char * c, cmd * current){
    
    int i = 0;
    int exist = -1;
    int exit = 0;
    
    int pid = 0;
    int priority = 0;
    
    //read 'nice'
    if(c[i++]=='n'  &&
       c[i++]=='i'  &&
       c[i++]=='c'  &&
       c[i++]=='e'  ){
        
        while(c[i] == ' ' && i<MAX_COMMAND_LENGTH){i++;}
        
//        printf("\nafter inicial spaces ");
//        printInt(exit);
        
        while (c[i] != ' ' && !exit && i<MAX_COMMAND_LENGTH) {
            
            if(c[i] >= '0' && c[i] <= '9'){
                pid = (c[i]-'0')+pid*10;
            }else{
                exit = 1;
            }
            i++;
        }
        
        while(c[i] == ' ' && !exit && i<MAX_COMMAND_LENGTH){i++;}
        
        
        while (c[i] != ' ' && c[i] != '\0' && !exit && i<MAX_COMMAND_LENGTH) {
            
            if(c[i] >= '0' && c[i] <= '9'){
                priority = (c[i]-'0')+priority*10;
            }else{
//                //testing printf
//                printf("\n entered exit cond at i = ");
//                printInt(i);
//                printf(" with c[i]: ");
                putChar(c[i]);
                exit = 1;
            }
            i++;
        }
        
//        //testing printf
//        printf("\n\ngot pid ");
//        printInt(pid);
//        printf("\ngot priority ");
//        printInt(priority);
        
        //while(c[i] == ' ' && !exit && i<MAX_COMMAND_LENGTH){i++;} //by how it is implemented this should not happen
        
        if(c[i] == '\0' && !exit && i<=MAX_COMMAND_LENGTH){
            exist = NICE;
            //current->commandId //not necessary
            current->priority = priority;
            current->stackSize = pid;               //fakes the stackSize as if it was pid for the syscall
            
        }
        
    }
    
    return exist;
}


////deprecated
//int getCmd(char *command){
//    int exist = -1;          //if it matches an existing command
//
//    //exist check
//    for(int i = 0 ; i < PCOUNT && exist == -1 ; i++){
//        if(strcmp(command, c[i]) == 0){
//            exist = i;
//        }
//    }
//
//    return exist;
//}

