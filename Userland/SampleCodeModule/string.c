#include "string.h"

int strcmp(char * str1, char * str2) {
	while(1) {
		if (*str1 != *str2) {
			return *str1 < *str2 ? -1 : 1;
		} else if (*str1 == '\0') {
			return 0;
		}
		str1++;
		str2++;
	}
}

char* intToCharA(char* digits, int number){
    
    char inverted[20];
    int i = 0;
    int j = 0;
    int n = number;
    
    //get digits but inverted
    do{
        inverted[i] = (char) ('0' + (n % 10) );
        
        n /= 10;
        i++;
    }while(n > 0);
    i--;
    
    //invert inverted and save in digits
    while(j <= i){
        digits[j] = inverted[i-j];
        j++;
    }
    
    digits[j] = '\0';
    
    return digits;
}

void trim(char * str){
    int last = strlen(str) - 1;
    while(str[last] == ' '){
        str[last--] = '\0';
    }
    return;
}

int strlen(char * str) {
    int i = 0;
    while(*(str+i)) {
        i++;
    }
    return i;
}

char * reverseString (char * str, char * inverted){
    int i;
    int len = strlen(str);

    for (i = 0; i < len; i++){
        inverted[len - 1 - i] = str[i];
    }
    inverted[len] = '\0';
    return inverted;
}

uint64_t stringToInt (char * str){
    uint64_t len = strlen(str);
    uint64_t index, num = 0;

    for (index = 0; index < len; index++){
        num *= 10;
        if (str[index] < '0' || str[index] > '9') return 0;
        num += str[index] - '0';
    }
    return num;
}
