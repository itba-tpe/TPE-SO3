
#include "philosophers.h"

mutex mutexTable;
mutex deleteLock;       //So that only one delete can occur at a time
mutex countLock;        //for the philCount variable
philosopher phil[MAXPHIL];

int philCount = 0;
int runPhil;

void philosophersIntro(){
    printf("\nImplementation of Dining Philosopher Problem!\n");
    printf("Press a to ADD A PHILOSOPHER\n");
    printf("Press d to DELETE A PHILOSOPHER\n");
    printf("Press p to PRINT the state of the philosophers\n");
    printf("Press q to QUIT at any time\n");

    printf("\nPlease be patient, it will take some time to refresh correctly\n");
}

void initPhilosophers(){
    runPhil = 1;
    philCount = 0;

    philosophersIntro();

    mutexTable = getMutex("mutexTable");
    countLock = getMutex("countLock");
    deleteLock = getMutex("deleteLock");

    for(int i = 0 ; i < MAXPHIL ; i++){
        if (i < MINPHIL){
          addPhilosopher();
        } else {
            phil[i].seat = AVAILABLE;
        }
    }

    char c;
    while (runPhil){

        c = getChar();
        if (c == 'q'){
            int i;
            runPhil = 0;

            wait(countLock);
            int phils = philCount;
            post(countLock);

            // Unblock any process that was locked waiting for forks so that it can leave the loop
            for (i = 0; i < phils; i++){
                post(phil[i].mutex);
                post(phil[i].mutex);
            }

        } else if (c == 'a'){
            if (philCount < MAXPHIL) addPhilosopher();

        } else if (c == 'd'){
            wait(deleteLock);
            if (philCount > MINPHIL) deletePhilosopher();
            post(deleteLock);
        } else if (c == 'p'){
            wait(countLock);
            int phils = philCount;
            post(countLock);
            wait(mutexTable);
            printTableSnapshot(phils);
            post(mutexTable);
        }

    }

    deleteMutex(mutexTable);
    deleteMutex(deleteLock);
    deleteMutex(countLock);

    return;
}

void philosophy(){
    int i = findSeat();
    if (i == -1) return;

   // This so that the next time the process does a wait because it doesnt
    wait(phil[i].mutex);

    while (runPhil && phil[i].state != LEAVING){
        if (runPhil && phil[i].state != LEAVING) think(i);
        if (runPhil && phil[i].state != LEAVING) takeForks(i);
        if (runPhil && phil[i].state != LEAVING) eat(i);
        if (runPhil && phil[i].state != LEAVING) putForks(i);
    }
    // Me aseguro que llegue antes al wait de deletePhilosopher
    sleep(1);
    post(phil[i].mutex);
}

int findSeat() {
    int i;
    for (i = 0; i < MAXPHIL; i++){
        //Dont want many philosophers searching for a seat -> Race condition to see who gets
        wait(mutexTable);
        if (phil[i].seat == AVAILABLE || phil[i].seat == RESERVED){
            phil[i].seat = TAKEN;
            post(mutexTable);
            return i;
        }
        post(mutexTable);
    }
    return -1;
}

void takeForks(int i){
    wait(mutexTable);
    phil[i].state = HUNGRY;
    test(i);
    post(mutexTable);
    wait(phil[i].mutex);        //block if forks where not aquired
}

void putForks(int i) {
    wait(mutexTable);
    if (phil[i].state != LEAVING) phil[i].state = THINKING;

    // Si estan bloqueados lo de la derecha o izquierda los debloquea si ya pueden comer
    test(leftPhil(i, MAXPHIL));
    test(rightPhil(i, MAXPHIL));
    post(mutexTable);
}

//Function to see if i can start eating --> can start when neither the right or the left are eating
void test(int i) {
    wait(countLock);
    int left = leftPhil(i, philCount);
    int right = rightPhil(i, philCount);
    post(countLock);

    //Will eat if im hungre and my neighbors arent eating
    if ((phil[i].state == HUNGRY && phil[left].state != EATING && phil[right].state != EATING) || phil[i].state == LEAVING) {
        if (phil[i].state != LEAVING) phil[i].state = EATING;
        //This post will either unblock a waiting HUNGRY philosopher,
        //or a process that was waiting to LEAVE (called by the neighboring process which was eating)
        post(phil[i].mutex);
    }
}

void think(int i){
    sleep(THINKTIME(i));
}

void eat(int i) {
    sleep(EATTIME(i));
}

//This funtion requiered the countLock mutex before executing
//This lock was created so this funtion could add philosophers while the other ones are still doing there own thing
void addPhilosopher(){
    //When i add a philosopher the only thing that shouldnt change is the counter
    //Dont want the whole table to stop eating, etc
    wait(countLock);
    int addPhil = philCount++;
    char * id = addPhilosopherAt(addPhil);
    post(countLock);


    int right = rightPhil(addPhil, addPhil+1);
    int left = leftPhil(addPhil, addPhil+1);

        // Gotta see if the left or right side can eat now that I added a fork next to it
        // If the phil is EATING, I dont care
    test(left);
    test(right);

    createProcess(id, (uint64_t)&philosophy, MIN_PRIORITY - 3, ONE_BLOCK, !FOREGROUND, STDIN, STDOUT);
    printf("\nAdding philosopher...\n");
}

char * addPhilosopherAt(int i){

    char philId[2];
    char * id = intToCharA(philId, i);

    phil[i].id = id;
    phil[i].seat = RESERVED;
    phil[i].state = THINKING;
    phil[i].mutex = getMutex(id);

    return id;
}

//This funtion requiered the countLock mutex before executing
void deletePhilosopher(){
    /* Will remove the last one added

    If either the left or the right is not eating, I can just remove it
    Problem comes when both left and right are eating since they are using
    the forks on both sides of the philosopher I want to remove

    */
    wait(countLock);
    int delPhil = philCount-1;
    post(countLock);

    int left = leftPhil(delPhil, delPhil + 1);
    int right = rightPhil(delPhil, delPhil + 1);

    wait(mutexTable);
    phil[delPhil].state = LEAVING;
    phil[delPhil].seat = AVAILABLE;
    post(mutexTable);
    wait(countLock);
    philCount--;
    post(countLock);

    // Have to check if the ones next to me were blocked waiting for my fork
    // Now that I am gone, can they eat?
    wait(mutexTable);
    test(left);
    test(right);
    post(mutexTable);
    //Unlock incase it was locked because it was hungry
    post(phil[delPhil].mutex);
    printf("\nDeleting philosopher...\n");
}

void printTableSnapshot(int phils){
    //if I am ending, dont want to print
    if (!runPhil) {
        return;
    }
    printf("\n----------------START---------------\n");

    printf("\nPHILOSOPHERS:");
    //philosophers

    for (int i = 0 ; i < phils ; i++ ){
        printf("\n");
        printf("Philosopher: ");
        printInt(i+1);
        printf(": ");

        switch (phil[i].state) {
            case THINKING:
                printf("THINKING\n");
                break;
            case HUNGRY:
                printf("HUNGRY\n");
                break;
            case EATING:
                printf("EATING\n");
                break;

            default:
                printf("AWAY\n");
                break;
        }
    }
    printf("\n-----------------END----------------\n");
}

int leftPhil(int i, int curPhil) {
    return (i+curPhil-1)%curPhil;
}

int rightPhil(int i, int curPhil) {
    return (i+1)%curPhil;
}
