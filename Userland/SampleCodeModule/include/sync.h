#ifndef SYNC_H
#define SYNC_H

#include <stdint.h>

//We define the type mutex as the pointer to a uint64_t to avoid access to kernel memory
typedef uint64_t * mutex;

mutex getMutex(const char * name);

void wait(mutex m);

void post(mutex m);

void deleteMutex(mutex m);

#endif