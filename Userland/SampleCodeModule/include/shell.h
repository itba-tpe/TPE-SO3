#ifndef SHELL_H
#define SHELL_H

#include "general.h"
#include "modules.h"
#include "string.h"
#include "stdio.h"
#include "syscalls.h"
#include "sync.h"
#include "prodcons.h"
#include "extra.h"
#include "sysWrappers.h"
#include "priorityDemo.h"
#include "memoryDemo.h"
#include "pipeDemo.h"
#include "philosophers.h"

//Change this numner to whatever you feel like
#define MAX_COMMAND_LENGTH 1024
#define SHELL_STACK_SIZE 2*ONE_BLOCK
#define SHELL_PRIORITY MAX_PRIORITY
#define SHELL_FOREGROUND 1

//some command defines
#define HELP 0
#define TIME 1
#define PONG 2
#define PS  3
#define PRODCONS 4
#define CLEAR 5
#define EXIT 6
#define PIPEDEMO 7
#define MEMORYDEMO 8
#define WHILEONE 9
#define DOUBLELOCK 10
#define PRINTMEM 11
#define NICE 12
#define PRINTTEXT 13
#define PRINTREVERSE 14
#define BUDDYDEMO 15
#define PRIORITYDEMO 16
#define PRINTPCB 17
#define PHILOSOPHERS 18
#define PCOUNT 19

#define PIPECHAR '!'

//---------------------------------------------------
//struct to be used

typedef struct cmd cmd;

//struct command
struct cmd{
    int commandId;
    int priority;
    int stackSize;
    char fg;
    
    cmd * next;
};

//---------------------------------------------------
//functions

void shell_init();
int shell_execute(char *command);

int getCommand(char *command);
int validateCommand(char *command, cmd *process);
void freeCommands(void);
int runCommand(cmd *command, int input, int output);

int isForeground(char * command);

//nice is the only command with parameters, therefore requires another type of parsing
//if in the future more commands require complex parsing, create an array with parsing functions.
int isNice(char * c,cmd * current);

////deprecated
//int getCmd(char *command);

#endif
