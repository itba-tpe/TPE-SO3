#ifndef SYSWRAPPERS_H
#define SYSWRAPPERS_H

#include <stdint.h>
#include "syscalls.h"
#include "processInfo.h"

void printMemory(void);
void sleep();
void drawBlackRect(uint64_t x, uint64_t y);
void createProcess(char * name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground, uint64_t fdin, uint64_t fdout);
void changeProcessPriority (uint64_t pid, uint64_t newPriority);
uint64_t getPid(void);
void printPCB(void);
void popen(uint64_t fd[2]);
void pclose(uint64_t fd);

#endif
