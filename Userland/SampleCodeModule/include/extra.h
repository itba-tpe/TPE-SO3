#ifndef EXTRA_H
#define EXTRA_H

#include <stdint.h>
#include "sync.h"
#include "stdio.h"
#include "string.h"

#define MAX_CHARACTERS 600

void whileOne(void);
void doubleLock(void);
void printEnteredText(void);
void printInvertedText (void);

int div(int n);
int ticksPerSecond(void);

#endif