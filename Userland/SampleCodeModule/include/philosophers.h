
#ifndef PHIL_H
#define PHIL_H

#include "general.h"
#include "modules.h"
#include "sync.h"
#include "stdio.h"
#include "string.h"
#include "sysWrappers.h"
#include "syscalls.h"
#include <stdint.h>

//some defines

#define MAXPHIL 20
#define MINPHIL 3

#define THINKING 0
#define HUNGRY 1
#define EATING 2
#define LEAVING 3
#define AWAY 4

//seat status
#define AVAILABLE   0
#define RESERVED    1
#define TAKEN       2

#define EATTIME(i) (i%2 + 2)
#define THINKTIME(i) (i%3 + 2)


typedef struct {
	char * id;
	char seat;
    int state;
    mutex mutex;
} philosopher;

void initPhilosophers();
void philosophersIntro();
void philosophy();
int findSeat();
void think(int i);
void eat(int i);
void takeForks(int i);
void putForks(int i);
void test(int i);
void printTableSnapshot(int phils);
void photographer();
char * addPhilosopherAt(int i);
void addPhilosopher();
void deletePhilosopher();
int leftPhil(int i, int curPhil);
int rightPhil(int i, int curPhil);

#endif
