#ifndef GENERAL_H
#define GENERAL_H

#define MIN_PRIORITY 19
#define MAX_PRIORITY 0
#define ONE_BLOCK 4096

#define STDIN 0
#define STDOUT 1

#define FOREGROUND 1

#endif