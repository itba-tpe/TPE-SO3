#ifndef PRODCONS_H
#define PRODCONS_H

#include "general.h"
#include "pipes.h"
#include "sync.h"
#include "stdio.h"
#include "string.h"
#include "sysWrappers.h"
#include <stdint.h>

#define MAX_PROD 6
#define MAX_CONS 6
#define BACKGROUND !FOREGROUND
#define BUFFSIZE 25
#define PIPE "prodConsPipe"

void initProdCons();
void intro();
void resetVariables();

void createProducer();
void producer();

void createConsumer();
void consumer();

#endif
