#ifndef PROCESSINFO_H
#define PROCESSINFO_H

typedef struct {
	char * name;
	uint64_t priority;
	uint64_t memSize;
	uint64_t foreground;
	uint64_t fdInput;
	uint64_t fdOutput;
	uint64_t rip;
} processInfo;

#endif 