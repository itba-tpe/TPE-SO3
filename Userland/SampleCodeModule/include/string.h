#ifndef STRING_H
#define STRING_H

#include <stdint.h>

int strcmp(char * str1, char * str2);
int strlen(char * str);
void trim(char * str);
char* intToCharA(char* digits, int number);
char * reverseString (char * str, char * inv);
uint64_t stringToInt (char * str);

#endif
