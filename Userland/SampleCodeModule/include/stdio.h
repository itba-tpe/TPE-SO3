#ifndef STDIO_H
#define STDIO_H

void printInt(int num);
char getChar();
void putChar(char c);
void printf(char * str);
void scanf(char * str, int bytes);
char getCharWithZero();
void hidden_scanf(char *str, int bytes);

#endif
