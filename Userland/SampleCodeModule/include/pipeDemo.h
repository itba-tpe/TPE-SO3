#ifndef PIPEDEMO_H
#define PIPEDEMO_H

#include "string.h"
#include "sysWrappers.h"
#include "stdio.h"
#include "pipes.h"
#include "general.h"

#define BUF_SIZE 50
#define PIPE1 "pipe1"
#define PIPE2 "pipe2"

void initPipesDemo();
void introduction();
void createMessengers();
void messenger1();
void messenger2();

#endif
