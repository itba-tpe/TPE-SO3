#ifndef MEMORYDEMO_H
#define MEMORYDEMO_H

#include "general.h"
#include "sysWrappers.h"
#include "syscalls.h"
#include "stdio.h"

#define MEM_ALLOCATED_AMOUNT 40
#define INITIAL_MEM 4000
#define OFFSET 1500

#define BLOCK_OFFSET 10
#define PTR_AMOUNT 6

void memoryDemo();
void testBuddyAllocation();
uint64_t allocatePtr (uint64_t blockAmount);
void freePtrs (uint64_t ptrs[], uint64_t length);

#endif