

#ifndef SYSCALLS_H
#define SYSCALLS_H

#include <stdint.h>
#include "processInfo.h"
#include "general.h"

int os_ticks(void);
int os_sec(void);
uint64_t read(uint64_t fd, char *buffer, uint64_t size);
uint64_t write(uint64_t fd, char *buffer, uint64_t size);
uint64_t * os_time(void);
uint64_t os_draw(uint64_t x, uint64_t y, uint64_t red,uint64_t green,uint64_t blue);
void os_clear(void);
void os_beep();
void os_unbeep();
void os_create(processInfo * process);
void os_kill(uint64_t pid);
uint64_t os_ps(void);
void os_sleep(uint64_t timeUnit);
void os_popen(uint64_t fd[2]);
void os_pclose(uint64_t fd);
void * os_malloc(int size);
void os_free(void * ptr);
uint64_t * os_getMutex(const char *);
void os_wait(uint64_t * mutex);
void os_post(uint64_t * mutex);
void os_deleteMutex(uint64_t * mutex);
uint64_t os_open_pipe (char * name);
void os_print_memory(void);
void os_print_PCB(void);
void os_change_priority(uint64_t pid, uint64_t newValue);
uint64_t os_get_pid(void);
void os_nice(uint64_t pid, int newValue);

#endif
