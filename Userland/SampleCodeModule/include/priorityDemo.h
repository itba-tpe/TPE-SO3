#ifndef PRIORITYDEMO_H
#define PRIORITYDEMO_H

#include <stdint.h>
#include "sysWrappers.h"
#include "string.h"
#include "stdio.h"
#include "general.h"
#include "sync.h"

#define SLEEP 1000
#define MAXCHARACTER 200
#define PRIORITY_SIZE 2
#define PROCESSA_ID "X"
#define PROCESSB_ID "B"

void priorityDemo(void);
void processA(void);
void processB(void);
void printLetter(char * letter);
uint64_t getPriority(char * id);
void syncPriorityChange(char * id);
void miniSleep(void);
void introMessage(void);

#endif