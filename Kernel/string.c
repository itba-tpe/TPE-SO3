#include "string.h"

int strcmp(char * str1, char * str2) {
	while(1) {
		if (*str1 != *str2) {
			return *str1 < *str2 ? -1 : 1;
		} else if (*str1 == '\0') {
			return 0;
		}
		str1++;
		str2++;
	}
}

//FIXME maybe we dont need it, im not sure. Strings in C are weird
//Makes a copy of a string
char * duplicateString(const char * name) {
	//We get the size of the original string
	int len = strlen(name);
	len++;		//strlen doesn't account for the \0 at the end

	//We allocate enough memory for the duplicate
	char * dup = (char *) allocate(len);

	//Verify success
	if (dup == (char *) -1) {
		return (char *) -1;
	}

	//now we copy the string
	dup = (char *) memcpy((void *) dup, (void *) name, len);

	return dup;
}

char* intToCharA(char* digits, uint64_t number){
    
    char inverted[20];
    int i = 0;
    int j = 0;
    uint64_t n = number;
    
    //get digits but inverted
    do{
        inverted[i] = (char) ('0' + (n % 10) );
        
        n /= 10;
        i++;
    }while(n > 0);
    i--;
    
    //invert inverted and save in digits
    while(j <= i){
        digits[j] = inverted[i-j];
        j++;
    }
    
    digits[j] = '\0';
    
    return digits;
}

//writes a string in a charA without finishing it with null
int writeString(char * dir, char * name){
    
    int iter = 0;
    
    while(name[iter] != '\0'){
        dir[iter] = name[iter];
        iter++;
    }
    
    return iter;
}

//Returns size of the string
int strlen(const char * str) {
    int i = 0;
    while(*(str+i)) {
        i++;
    }
    return i;
}
