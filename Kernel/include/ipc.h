#ifndef IPC_H
#define IPC_H

#include <stdint.h>
#include "string.h"
#include "scheduler.h"
#include "vesaDriver.h"

#include "file.h"

#define MAX_PIPES 	20	//To aliviate some pressure on our memory and make faster response times for the write and read it will be static


typedef struct pipe pipe;

//Structure that hold the buffer to which we are going to write and read from and some type of identifier the programmer decides
struct pipe {
    char * id;
    char used;
    struct buffer * buffer;
};

int open(char * id);
int findPipe(char * id);
void close(struct buffer * buff);
int findFree(void);
int findBuffer(struct buffer * buff);
#endif
