#ifndef __FILE_H
#define __FILE_H

#include "memoryManager.h"
#include "vesaDriver.h"
#include "scheduler.h"


#define MAX_BUFFER 1024    //Based on the scope of the project, this seems enough

//---------------------------------------------------
//structs to be used
//myRead & myWrite must have the format:
// int myRW(file *d, char * string, uint64_t length);
//return is the number of bytes written/read

typedef struct file file;
typedef struct buffer buffer;

typedef struct pidNode pidNode;

struct file{
    struct buffer * fileStruct;
    int (*myRead) (file *, char *, uint64_t);
    int (*myWrite) (file *, char *, uint64_t);
};

struct buffer{
    char * buf;
    int cursorR;
    int cursorW;
    int size;
    int avail;
    int userCount;
    struct pidNode * queue;
};

struct pidNode {
    int pid;
    pidNode * next;
};

//---------------------------------------------------
//file related functions

file * createBuffer();

file * createFileFromBuffer(buffer * buff);

file * copyFile(file * f);

file * getScreen();

file * getKeyboard();

int closeFile(file * f);

//---------------------------------------------------
//buffer functions
buffer * newBuffer();

int closeBuffer(buffer * buff);

//---------------------------------------------------
//read and write functions

//these 2 wrap the myR&W
int readFile(file * f, char * buffer, uint64_t len);
int writeFile(file * f, char * buffer, uint64_t len);

int writeToScreen(file * f, char * buffer, uint64_t len);
int readFromKeyboard(file * f, char * buffer, uint64_t len);

int writeToBuffer(file * f, char * buffer, uint64_t len);
int readFromBuffer(file * f, char * buffer, uint64_t len);

#endif
