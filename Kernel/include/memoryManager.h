#ifndef MEMORYMANAGER_H
#define MEMORYMANAGER_H

#include <stdint.h>
#include "vesaDriver.h"
#include "general.h"

#define MEM_SIZE 0x200000					//Equivalent to = 2^21 
#define PAGE_SIZE 0x1000					//Pages of 4KB
#define MEM_ARRAY_SIZE (MEM_SIZE/PAGE_SIZE)	//Array of 1024 positions
#define MAX_BUDDIES 21						//Maximum buddies is actually log2(MEM_ARRAY_SIZE) but we dont have a log function! 

#define YES 1
#define NO 0

#define WAITING 8000

typedef struct {
	char isAllocated;				//Whether the block is allocated or free
	uint64_t size;					//Size of the memory block I am working with (may vary now)
	uint64_t buddies[MAX_BUDDIES];	//"History" of my buddies
	uint64_t currentBuddy;			//Index of the buddies array = block's current buddy
} memBlock;

uint64_t allocate (uint64_t size);
uint64_t free (uint64_t ptr);
void partitionBlock(uint64_t startIdx, uint64_t buddyIdx);
uint64_t getBuddyOffset(uint64_t blockSize);
uint64_t mergeMemory(uint64_t index);
void printBuddyError();

void printMemory();
void wait();
void printAllocationError(uint64_t size, uint64_t pages);
void printFreeError();

#endif