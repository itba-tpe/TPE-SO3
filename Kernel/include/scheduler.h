
#ifndef __SCHEDULER_H
#define __SCHEDULER_H

#include <stdint.h>
#include <stddef.h>
#include "string.h"
#include "vesaDriver.h"
#include "memoryManager.h"
#include "systemCalls.h"
#include "mutex.h"
#include "file.h"
#include "processInfo.h"
#include "general.h"

//stack size used in init
#define INIT_MEM ONE_BLOCK

//process state enum
#define BLOCKED 0
#define READY   1
#define ACTIVE  2
#define DEAD    3

//max quantity of processes
#define MAXPROCESS 20

//max priority
#define MAXPRIORITY 20

//max file descriptors
#define MAXFD 10

//max for line in ps
#define MAXLINE 100
//must be at least (MAXPROCESS+1)*MAXLINE
#define MAXSIZE ((MAXPROCESS+1) * MAXLINE)

//---------------------------------------------------
//structs to be used

typedef struct process process;
typedef struct scheduleNode scheduleNode;
typedef struct stackFrame stackFrame;

//struct process saves all information about a process, rsp is the most important
//lower priority number -> higher importance
struct process{
    uint64_t pid;
    char used;
    char * name;
    uint64_t stack;
    uint64_t rsp;
    char state;
    uint64_t priority;
    uint64_t resMemory;
    char foreground;
    struct file * fd[MAXFD];
};

//struct to create a circular linked list to represent the round robin
struct scheduleNode{
    uint64_t pid;
    scheduleNode * next;
};

//Stack Frame
struct stackFrame {
    //Registers restore context
    uint64_t rax;
    uint64_t r15;
    uint64_t r14;
    uint64_t r13;
    uint64_t r12;
    uint64_t r11;
    uint64_t r10;
    uint64_t r9;
    uint64_t r8;
    uint64_t rsi;
    uint64_t rdi;
    uint64_t rbp;
    uint64_t rdx;
    uint64_t rcx;
    uint64_t rbx;
    
    //iretq hook
    uint64_t rip;
    uint64_t cs;
    uint64_t eflags;
    uint64_t rsp;
    uint64_t ss;
    uint64_t base;
};

//---------------------------------------------------
//IMPORTANT, CALL START SCHEDULER BEFORE DOING ANY OTHER TASK BECAUSE FAILING TO DO SO WILL COLAPSE THE ENTIRE SCHEDULER
void scheduler_init(uint64_t rip);

//---------------------------------------------------
//important functions

uint64_t next(uint64_t curRSP);

uint64_t first();

uint64_t createProcess(uint64_t name, uint64_t rip, uint64_t priority, uint64_t memSize, uint64_t foreground, uint64_t fdInput, uint64_t fdOutput);

uint64_t freeProcess(uint64_t pid);

uint64_t listAllProcess(void);

int changeState(uint64_t pid,char state);

int unblock(int pid);

int block(int pid);

int getRunningPid(void);

void changePriority(uint64_t pid, uint64_t priority);

void nice(uint64_t pid, int value);

//---------------------------------------------------
//internal functions

uint64_t getPriorityFromNice(int value);

int getDifference(uint64_t old, uint64_t new);

int addNodes(uint64_t pid, uint64_t quantity);

void remNodes(uint64_t pid, uint64_t quantity);

void runProcess(uint64_t entryPoint);

uint64_t getFreeProcess(void);

scheduleNode * addRec(scheduleNode * currentNode, uint64_t pid, int interval,int currentOrder, unsigned int times);

uint64_t addQuantity(uint64_t priority);

void _contextSwitch();

void _killProcess(uint64_t pid);

uint64_t getPid(uint64_t iid);

int getFreeFD(int iid);

int assignFDToProcess(struct file * f, int iid);

struct file * getFileDecriptor(int fd);

int openFD();

int closeFD(int fd);

int cleanFDTable();

int openPipe(uint64_t filDes[2]);

void printList(void);

#endif
