#ifndef GENERAL_H
#define GENERAL_H

//#define MIN_PRIORITY 19
//#define MAX_PRIORITY 0
#define ONE_BLOCK 0x1000

#define STDIN 0
#define STDOUT 1

#define FOREGROUND 1

//TRUE and FALSE
#define FALSE 0
#define TRUE !FALSE

#endif