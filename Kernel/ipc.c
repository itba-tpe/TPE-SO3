#include "ipc.h"
#include "vesaDriver.h"



//Our pipes, all the data will be initialized with zeros so no problems here m8
static pipe myPipes[MAX_PIPES] = {{0}};

//We get the int for the pipe with this id
int open(char * id) {

	//We search for the pipe with indicated id on our list
	int i = findPipe(id);

	//If we couldn't find it we just create it
	if (i == -1) {
		
		//We search for a free space in our array
		i = findFree();

		//If there is no space left, we just return -1
		if (i == -1) {
			return -1;
		}

		//Otherwise, we load our info into this process structure

		//We duplicate the string
		myPipes[i].id = duplicateString(id);

		if ((uint64_t) myPipes[i].id == -1) {
			return -1;
		}
        
        myPipes[i].used = 1;                            //Set it as used
        myPipes[i].buffer = newBuffer(MAX_BUFFER);      //create the new buffer
	}

	//We return the user the int that represents this pipe
	return assignFDToProcess(createFileFromBuffer(myPipes[i].buffer), getRunningPid()%MAXPROCESS);
}

//Internal function
int findPipe(char * id) {

	//A simple search using strcmp
	for (int i = 0; i < MAX_PIPES; ++i) {
		if (myPipes[i].used != 0) {
			if (strcmp(myPipes[i].id, id) == 0) {
				return i;
			}
		}
	}
	return -1;
}

int findBuffer(buffer * buff) {

    for (int i = 0; i < MAX_PIPES; ++i) {
        if (myPipes[i].used != 0) {
            if (myPipes[i].buffer == buff) {
                return i;
            }
        }
    }
    return -1;
}

//Frees a slot for a new pipe
void close(buffer * buff) {

	//We find the pipe
	int i = findBuffer(buff);

	//If we actually found something
	if (i != -1) {

		//We mark it as no longer used
		myPipes[i].used = 0;

		//We free the space allocated to it's identifier
		free((uint64_t) myPipes[i].id);

		//unblock all queued nodes done in file.c
	}

	//Else we just return
	return;
}

//Returns i of free slot for the pipe
int findFree(void){
    
   	int idx = -1;
    
    for(int i = 0 ; idx == -1 && i < MAX_PIPES ; i++){
        if(myPipes[i].used == 0){
            myPipes[i].used = 1;
            idx = i;
        }
    }
    
    return idx;
}
