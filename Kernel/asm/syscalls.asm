GLOBAL sys_delete_process

EXTERN freeProcess

SECTION .text

;uint64_t pid (rdi)

sys_delete_process:
	push rbp
	mov rbp, rsp
	cli
	call freeProcess
	sti
	cmp rax, 0x01
	je .halt

.continue:
	mov rsp, rbp
	pop rbp
	ret

.halt:
	int 20h
	jmp .continue