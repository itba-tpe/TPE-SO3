#include "memoryManager.h"

static void * const memStartAddress = (void *)0x900000;		
static memBlock memory[MEM_ARRAY_SIZE] = {{0, MEM_SIZE, {0}, -1},{0, 0, {0}, -1}};	//TODO see how to initialize this

//------------------------------------------ ALLOCATE -------------------------------------------------

uint64_t allocate (uint64_t size){
	//Verify if input is legal
	if (size <= 0 || size > MEM_SIZE) {
		return -1;
	}
	uint64_t allocIdx = 0;		// Index which corresponds to the memory I will allocate

	//Find the first block big enough and free to allocate
	while (allocIdx < MEM_ARRAY_SIZE && (memory[allocIdx].size < size || memory[allocIdx].isAllocated)){
		allocIdx++;
	} 
	
	//Not enough space to allocate specified amount
	if (allocIdx == MEM_ARRAY_SIZE){
		return -1;
	}

	memBlock * allocBlock = &memory[allocIdx];	//To facilitate
	uint64_t buddyOffset;						//Amount I have to add to allocIdx to reach its buddy

	//Amount of PAGE_SIZE sized blocks that need to be allocated 
	uint64_t blocks = size / PAGE_SIZE;
	if (size % PAGE_SIZE != 0) {		
		blocks++;
	}

	//Partition until I reach the smallest partition big enough, dont want to partition if its the smallest already
	if (allocBlock->size > PAGE_SIZE){

		// Divide the block in half if it still fits in half the block's size
		while (allocBlock->size/2 >= PAGE_SIZE && (allocBlock->size)/2 >= size){
			buddyOffset = getBuddyOffset(allocBlock->size);
			partitionBlock(allocIdx, allocIdx + buddyOffset);
		}	
	
		// Leave the while loop = ready to allocate
		for (int i = allocIdx; blocks > 0 && i < MEM_ARRAY_SIZE; blocks--, i++){
			memory[i].isAllocated = YES;
		}
	} else {
		//Only need to allocate the one I'm on
		allocBlock->isAllocated = YES;
	}
	//Now we get the physical address of the block we just allocated and return it
	uint64_t returnPtr = (uint64_t)(memStartAddress + allocIdx*PAGE_SIZE);
	return returnPtr;
}

//------------------------------------------ FREE -------------------------------------------------

uint64_t free (uint64_t ptr) {
	//We verify if the input is legal 

	if (ptr < (uint64_t) memStartAddress || ptr > (uint64_t) (memStartAddress + (MEM_ARRAY_SIZE - 1)*PAGE_SIZE)) {
		return -1;
	}

	//We get the page corresponding to this memoryAdress and we verify if it's the first page of a block
	uint64_t freeIdx = (ptr - (uint64_t)memStartAddress)/PAGE_SIZE;		//Dangerous if what i sent to free is not something we returned
	
	if ((ptr - (uint64_t)memStartAddress)%PAGE_SIZE != 0) {
		printFreeError();
		return -1;
	}

	memBlock * freeBlock = &memory[freeIdx];

	//Pointer we recieved is not the start of a block / is not allocated --> Error
	if (freeBlock->size == 0 && !freeBlock->isAllocated){
		printFreeError();
		return -1;		
	}

	freeBlock->isAllocated = NO;

	// Free all the blocks used besides the first one --> all following blocks with size = 0
	for (int i = freeIdx + 1; i < MEM_ARRAY_SIZE && memory[i].size == 0; i++){
		memory[i].isAllocated = NO;
	}
	
	//If the merge was successful, then we are done and the free was successful
	return mergeMemory(freeIdx);
}

//------------------------------------------ AUXILIARY -------------------------------------------------


void partitionBlock(uint64_t startIdx, uint64_t buddyIdx){
	memBlock * startBlock = &memory[startIdx];
	memBlock * buddyBlock = &memory[buddyIdx];

	//Updating block size
	uint64_t currSize = (startBlock->size);
	startBlock->size = currSize/2;
	buddyBlock->size = currSize/2;

	//Adding each others indexes to the history of buddies
	startBlock->buddies[++(startBlock->currentBuddy)] = buddyIdx;
	buddyBlock->buddies[++(buddyBlock->currentBuddy)] = startIdx;

	return;
}

uint64_t getBuddyOffset(uint64_t blockSize){
	return (blockSize/2)/PAGE_SIZE;	//Amount of blocks necesary to represent blockSize/2
}

uint64_t mergeMemory(uint64_t index){
	uint64_t mergeIdx, buddyIdx;

	//Has no buddy --> only happens if its the full memory block (supposing I recieved a valid index which I always will)
	if (memory[index].currentBuddy == -1) return 0;

	buddyIdx = memory[index].buddies[memory[index].currentBuddy];

	//Dont want to merge if my buddy is allocated or has another buddy they need to merge with before me
	if (memory[buddyIdx].isAllocated || memory[buddyIdx].size != memory[index].size) return 0;

	// I want mergeIdx to be the index where the merging will occur
	// For this to happen it must be the smallest amoung the two
	if (buddyIdx < index){
		mergeIdx = buddyIdx;
		buddyIdx = index;
	} else {
		mergeIdx = index;
	}

	memBlock * mergeBlock = &memory[mergeIdx];
	memBlock * buddyBlock = &memory[buddyIdx];

	mergeBlock->size += buddyBlock->size;	//Equivalent to x2
	(mergeBlock->currentBuddy)--;
	buddyBlock->size = 0;
	(buddyBlock->currentBuddy)--;

	// Have to check if I wanna merge with my previous buddy
	if (mergeBlock->currentBuddy != -1){
		if (mergeMemory(mergeIdx) == -1) return -1;
	}

	return 0;
}

void printMemory() {
	uint64_t index;
	nextLine();
	printWhiteString("ST:");
	nextLine();
	for (index = 0; index < MEM_ARRAY_SIZE; ++index) {
		if (memory[index].size != 0) {
			printDec(index);
			printWhiteString("->");
			printWhiteString("[");
			printHexa(memory[index].size);
			printWhiteString(",");
			printHexa(memory[index].isAllocated);
			printWhiteString("]");
			nextLine();

			// to simulate a very short sleep
			wait();
		}
	}
	printWhiteString("END");
}

void wait() {
	int i, j;
	for (i = 0; i < WAITING; i++){
		for (j = 0; j < WAITING; j++){
			//Just sleep
		}
	}
	return;
}

//------------------------------------------ ERROR FUNCTIONS -------------------------------------------------

void printAllocationError(uint64_t size, uint64_t pages){
	nextLine();
	printWhiteString("Size: ");
	printDec(size);
	printWhiteString(", Pages: ");
	printDec(pages);
	printWhiteString(", memDump: ");
	nextLine();
	printMemory();
	nextLine();
}

void printFreeError(){
	char * errorMsg = "\nERROR: FAILURE TO FREE\n";
	printWhiteString(errorMsg);
}

void printBuddyError(){
	char * errorMsg = "\nERROR: WRONG BUDDY WHEN MERGING\n";
	printWhiteString(errorMsg);
}