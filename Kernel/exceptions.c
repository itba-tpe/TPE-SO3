#include "naiveConsole.h"
#include "vesaDriver.h"
#include "exceptions.h"


char * exceptions[EXCEPTIONS]= {"Exception: Division by zero", "Exception: Invalid opcode"};
char * registers[REGISTERS]= {"R15", "R14", "R13", "R12", "R11", "R10", "R09", "R08", "RSI", "RDI", "RBP", "RDX", "RCX", "RBX", "RAX", "RSP", "RIP"};

void exceptionDispatcher(int exception, uint64_t * stackpointer) {
	
	if (exception == ZERO_EXCEPTION_ID)
		showException(0,stackpointer);

	if( exception == INVALID_OPCODE_ID)
		showException(1,stackpointer);

}

static void showException(int idx, uint64_t * stackpointer){
	nextLine();
	printWhiteString(exceptions[idx]);
	nextLine();
	printWhiteString("Registers:");
	nextLine();

	for(int i=0; i < REGISTERS; i++){
		printWhiteString(registers[i]);
		printWhiteString(" :");
		printHexa(stackpointer[i]);
		nextLine();
	}
	printWhiteString("Restarting shell...");
	nextLine();
}

