
#include "file.h"

static file screen = {'\0','\0',&writeToScreen};
static file keyboard = {'\0',&readFromKeyboard,'\0'};

// Will be called when creating a new file
file * createBuffer(){
    return createFileFromBuffer(newBuffer(MAX_BUFFER));
}

file * createFileFromBuffer(buffer * buff){
    
    if(buff == '\0')
        return (file *)'\0';
    
    file * newFile = (file *) allocate(sizeof(file));
    
    if(newFile == '\0')
        return newFile;
    
    newFile->fileStruct = buff;
    
    (newFile->fileStruct)->userCount++;
    
    newFile->myRead = &readFromBuffer;
    newFile->myWrite = &writeToBuffer;
    
    return newFile;
}

// Given a file *, will duplicate it (new process wants to use the pipe)
// The 2 file * will point to the same struct buffer (pipe) --> increments its userCount
file * copyFile(file * f){
    
    if(f == '\0' ||f == &screen || f == &keyboard)
        return f;
    
    file * newFile = (file *) allocate(sizeof(file));
    
    if(newFile == '\0')
        return newFile;
    
    newFile->fileStruct = f->fileStruct;
    newFile->myRead = f->myRead;
    newFile->myWrite = f->myWrite;
    
    (f->fileStruct)->userCount++;
    
    return newFile;
}

file * getScreen(){
    return &screen;
}

file * getKeyboard(){
    return &keyboard;
}

int closeFile(file * f){
    // Do not want to close the screen or keyboard file
    if(f == '\0' ||f == &screen || f == &keyboard)
        return 1;
    
    // Buffer will close ONLY if the current file was the last one referencing it
    if(f->fileStruct != '\0')
        closeBuffer(f->fileStruct);

    // File * is unique to the process so that I do want to close
    return (int) free((uint64_t)f);
    
}

//---------------------------------------------------
//buffer functions

buffer * newBuffer(int size){
    
    buffer * newBuff = (buffer *) allocate(sizeof(buffer));
    
    if(newBuff == '\0')
        return newBuff;
    
    newBuff->buf = (void *) allocate(size);
    
    if(newBuff->buf == '\0')
        return '\0';
    
    newBuff->cursorR = 0;
    newBuff->cursorW = 0;
    newBuff->size = size;
    newBuff->avail = size;
    newBuff->userCount = 0;
    
    return newBuff;
    
}

int closeBuffer(buffer * buff){
    
    buff->userCount--;
    
    // Will only close the struct buffer (aka pipe) associated with the
    // file * if no other process is using it
    // if a namedPipe has a buffer, it frees from its list
    if(buff->userCount <= 0){
        
        close(buff);
        
        //We unblock all queued nodes
        pidNode * current = (buff->queue);
        
        while(current != NULL) {
            int pid = current->pid;
            _killProcess(pid);
            pidNode * next = current->next;
            free((uint64_t) current);
            current = next;
        }
        
        free((uint64_t) buff);
    }
    
    
    return 1;
    
}

//---------------------------------------------------
//read and write functions

int readFile(file * f, char * buffer, uint64_t len){
    
    if(f->myRead == '\0')
        return -1;
    
    return (*(f->myRead))(f,buffer,len);
}

int writeFile(file * f, char * buffer, uint64_t len){
    
    if(f->myWrite == '\0')
        return -1;
    
    return (*(f->myWrite))(f,buffer,len);;
}

int writeToScreen(file * f, char * buffer, uint64_t len){
    
    int bytesWritten = 0;
    char c;
    
    while(len--) {
        c = *buffer;
        if (c == '\n') {
            nextLine();
        } else if (c == '\b') {
            deleteChar();
        } else if (c == -1) {
            nextLine();
        } else {
            printChar(c,255,255,255);
        }
        buffer++;
        bytesWritten++;
    }
    
    return bytesWritten;
}

int readFromKeyboard(file * f, char * buffer, uint64_t len){
    
    int bytesRead = 0;
    char c;
    
    while(len > 0 && (c = getChar())) {    //getChar returns 0 when the keboard buffer is empty
        buffer[bytesRead++] = c;
        len--;
    }
    
    return bytesRead;
}

int writeToBuffer(file * f, char * buffer, uint64_t len){
    int bytesWritten = 0;
    struct buffer * myBuffer = f->fileStruct;
    
    /*DEBUGGING: printing what to write
    nextLine();
    printWhiteString("to write in ");
    printDec((uint64_t) f);
    printWhiteString(" content: ");
    printWhiteString(buffer);
    END-DEBUG */
    
    //We check how much space in the buffer we have
    
    //If we are tying to write more than we can take, we dont write at all and notify the user, so the previous data is safe.
    //(In theory they still have this input available to try again later, but we cant say the same for the data already in the pipe)
    if (bytesWritten > myBuffer->avail) {
        return -1;
    }
    
    //Otherwise, we write to the buffer
    int write = myBuffer->cursorW;
    for (int i = 0; i < len; ++i) {
        (myBuffer->buf)[write] = buffer[i];
        //printChar(myBuffer->buffer[write], 255, 255, 255);
        write = (write + 1) % MAX_BUFFER;
        myBuffer->avail--;
    }
    
    myBuffer->cursorW = write;

    //Before we return we notify the first process in the queue that he can read something now
    while ( (myBuffer->queue) != NULL && unblock((myBuffer->queue)->pid) == -1) {
        //Lets free this node and try with the next one, because this one is dead
        pidNode * next = (myBuffer->queue)->next;
        free((uint64_t)myBuffer->queue);
        myBuffer->queue = next;
    }
    
    //If we sucessfully unblocked a non-dead process from the queue, we remove it from the queue
    if (myBuffer->queue != NULL) {
        pidNode * next = (myBuffer->queue)->next;
        free((uint64_t)(myBuffer->queue));
        myBuffer->queue = next;
    }
    
//    //DEBUGGING: printing buffer state
//    nextLine();
//    printWhiteString("WRITE: cursorR: ");
//    printDec((uint64_t) myBuffer->cursorR);
//    printWhiteString(" cursorW: ");
//    printDec((uint64_t) myBuffer->cursorW);
//    //END-DEBUG
    
    return bytesWritten;
}

int readFromBuffer(file * f, char * buffer, uint64_t len){
    int bytesRead = 0;
    struct buffer * myBuffer = f->fileStruct;
    
//    //DEBUGGING: printing what to read
//    nextLine();
//    printWhiteString("read from ");
//    printDec((uint64_t) f);
//    printWhiteString(" content: ");
//    for(int i = 0 ; i< len ; i++){
//        if(myBuffer->buf[myBuffer->cursorR+i] == '\n')
//            printWhiteString("\n");
//        else
//            printChar(myBuffer->buf[myBuffer->cursorR+i],255,255,255);
//    }
//    //END-DEBUG
    
    //If there are other proccesses waiting to read from this pipe, since we are polite, we wait until they are finished
    if (myBuffer->queue != NULL) {
        
        //Who are we?
        int pid = getRunningPid();

       
        //We add ourselves to the queue
        pidNode * node = (pidNode *) allocate(sizeof(pidNode));
        node->pid = pid;
        node->next = NULL;
        
        pidNode * current = myBuffer->queue;
        while(current->next != NULL) {
            current = current->next;
        }
        current->next = node;
        
        block(pid);
        _contextSwitch();
    }
    
    //If there is nothing to read we wait until there is
    if (myBuffer->cursorW == myBuffer->cursorR) {
        //Who are we?
        int pid = getRunningPid();
        
        //We add ourselves to the queue
        pidNode * node = (pidNode *) allocate(sizeof(pidNode));
        node->pid = pid;
        node->next = NULL;
        
        pidNode * current = myBuffer->queue;
        
        if (current == NULL) {
            myBuffer->queue = node;
        }
        
        block(pid);
        _contextSwitch();
    }
    
    //We read up to bytes (or until we run out of stuff to read)
    int write = myBuffer->cursorW;
    int read = myBuffer->cursorR;
    
    for (int i = 0; i < len && read != write; ++i, ++bytesRead) {
        buffer[i] = (myBuffer->buf)[read];
        read = (read + 1) % MAX_BUFFER;
        myBuffer->avail++;
    }
    
    myBuffer->cursorR = read;

    //If i left things to be read i unblock the next process
    
    if (myBuffer->cursorR != myBuffer->cursorW) {
        
        //Before we return we notify the first process in the queue that he can read something now
        while ( (myBuffer->queue) != NULL && unblock((myBuffer->queue)->pid) == -1) {
            //Lets free this node and try with the next one, because this one is dead
            pidNode * next = (myBuffer->queue)->next;
            free((uint64_t)myBuffer->queue);
            myBuffer->queue = next;
        }
        
        //If we sucessfully unblocked a non-dead process from the queue, we remove it from the queue
        if (myBuffer->queue != NULL) {
            pidNode * next = (myBuffer->queue)->next;
            free((uint64_t)(myBuffer->queue));
            myBuffer->queue = next;
        }
    }
    
//    //DEBUGGING: printing buffer state
//    nextLine();
//    printWhiteString("READ: cursorR: ");
//    printDec((uint64_t) myBuffer->cursorR);
//    printWhiteString(" cursorW: ");
//    printDec((uint64_t) myBuffer->cursorW);
//    //END-DEBUG
    
    return bytesRead;
}
